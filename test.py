from imports import *
from pydub.effects import compress_dynamic_range

c = Note(69).getSet(Chords.major7)
sc = Note(69).getSet(Scales.major)
prt = Part().addChord(c, 4, 2)
for n in sc: prt.addNote(n, 1)
prt.addNote(Note(69).getOct(1), 2)
prt.addChord(Note(69).getSet(Chords.minor7), 4)
seg = ViolinSampler.playPart(prt, 750)
seg.export("test.wav")