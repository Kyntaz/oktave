from math import log

class Note:
    def __init__(self, p):
        self.p = p
        
    def f(self):
        return 2**((self.p - 69) / 12) * 440
    
    def set_f(self, v):
        self.p = 69 + 12 * log(v/440, 2)
        return self

    def rep(self):
        return [
            'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'
            ][self.p % 12] + str(self.p // 12 - 1)

    def getOct(self, n):
        return Note(12*n + self.p)

    def getSet(self, ints):
        out = []
        for i in ints:
            out += [Note(self.p + i)]       
        return out