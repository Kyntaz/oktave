class Scales:
    major = [0,2,4,5,7,9,11]
    minor = [0,2,3,5,7,8,10] 
    pentaMaj = [0,2,4,7,9]
    pentaMin = [0,3,5,7,10] 

class Chords:
    major = [0,4,7]
    minor = [0,3,7] 
    major7 = [0,4,7,11] 
    dominant = [0,4,7,10] 
    minor7 = [0,3,7,10]

    @classmethod
    def makeChord(cls, sc, st, n=3, i=2):       
        cho = []; 
        for no in range(n):
            cho += [sc[(no*i + st) % len(sc)]]
        return cho