class Part:

    def __init__(self):
        self.seq = []

    def addNote(self, n, t, r=0):
        self.seq += [([n],t,r)]
        return self

    def addRest(self, t):
        self.seq += [([],t,0)]
        return self

    def addChord(self, c, t, r=0):
        self.seq += [(c,t,r)]
        return self

    def rep(self):
        rep = ""
        for k in self.seq:
            for n in k[0]:
                rep += "|" + n.rep()
            rep += "|:" + str(k[1]) + "|"
        return rep
