from pydub import AudioSegment

def pitchShift(seg, f1, f2):
    new_rate = int(seg.frame_rate * (f2/f1))
    return seg._spawn(seg.raw_data, overrides = { 'frame_rate': new_rate })