from models import Note, Part
from utils import pitchShift
from pydub import AudioSegment

class Sampler:
    def __init__(self, samples = {}):
        self.samples = samples

    def adSample(self, file, p):
        self.samples[p] = file
        return self

    def removeSample(self, p):
        del self.samples[p]
        return self
    
    def playNote(self, n):
        closestN = None
        for k in self.samples.keys():
            if closestN == None or abs(k - n.p) <= abs(closestN - n.p):
                closestN = k
        s = AudioSegment.from_wav(self.samples[closestN])
        return pitchShift(s, Note(closestN).f(), n.f()) - 10

    def playSet(self, ns):
        s = self.playNote(ns[0])
        for n in ns[1:]:
            s = s.overlay(self.playNote(n))
        return s

    def playPart(self, prt, q):
        tail = 0
        s = AudioSegment.empty()
        for p in prt.seq:
            ns = AudioSegment.silent(tail*q) + self.playSet(p[0])[:p[1]*q + p[2]*q + 25].fade_out(50)
            s = ns.overlay(s)
            tail += p[1]        
        return s