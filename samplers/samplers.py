from samplers import Sampler

PianoSampler = Sampler(
    {
        33: "Samples\Grand Piano\violin-a1.wav",
        45: "Samples\Grand Piano\violin-a2.wav",
        57: "Samples\Grand Piano\violin-a3.wav",
        69: "Samples\Grand Piano\violin-a4.wav",
        81: "Samples\Grand Piano\violin-a5.wav",
        93: "Samples\Grand Piano\violin-a6.wav",
        105: "Samples\Grand Piano\violin-a7.wav",
    })

HarpSampler = Sampler(
    {
        57: "Samples\Harp\harp-a3.wav",
        45: "Samples\Harp\harp-a2.wav",
        69: "Samples\Harp\harp-a4.wav",
        81: "Samples\Harp\harp-a5.wav",
        93: "Samples\Harp\harp-a6.wav",
        96: "Samples\Harp\harp-c7.wav",
    })

ViolinSampler = Sampler(
    {
        58: "Samples\Violin\\violin-a#3.wav",
        70: "Samples\Violin\\violin-a#4.wav",
        82: "Samples\Violin\\violin-a#5.wav",
        94: "Samples\Violin\\violin-a#6.wav",
    })